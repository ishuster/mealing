# Mealing



## Getting started

### Requisites

- [Brew](https://brew.sh)
- [Tuist](https://tuist.io)
- Xcode Version 13.2.1 (13C100)

Enter the right directory:

```sh
cd Mealing
```

`Brew` and `Tuist` will bet set up by running the following command:

```sh
./bootstrap
```

It will install `Brew` and `Tuist` and generate the `.xcworkspace`

After running the `bootstrap` script open the project workspace:

```sh
open Mealing.xcworkspace
```

However, it is recommended that you open the project with `Tuist`

```sh
tuist generate
```

It will generate the `.xcworkspace` and open it automatically. 

### Troubleshooting

If any issues arise when running `tuist generate` because of depencencies. Fetch them by doing:

```
tuist fetch
```

See [Tuist documentation](https://docs.tuist.io/tutorial/get-started)