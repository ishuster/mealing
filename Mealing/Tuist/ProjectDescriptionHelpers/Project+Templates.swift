import ProjectDescription

/// Project helpers are functions that simplify the way you define your project.
/// Share code to create targets, settings, dependencies,
/// Create your own conventions, e.g: a func that makes sure all shared targets are "static frameworks"
/// See https://docs.tuist.io/guides/helpers/

extension Project {
    /// Helper function to create the Project for this ExampleApp
    ///
    public static func app(name: String, platform: Platform, deploymentTarget: ProjectDescription.DeploymentTarget, externalDependencies: [String], organizationName: String, additionalTargets: [String]) -> Project {
        var dependencies = additionalTargets.map { TargetDependency.target(name: $0) }
        dependencies += externalDependencies.map { .external(name: $0) }
        var targets = makeAppTargets(name: name,
                                     platform: platform,
                                     deploymentTarget: deploymentTarget,
                                     dependencies: dependencies,
                                     organizationName: organizationName
        )
        targets += additionalTargets.flatMap({ makeFrameworkTargets(name: $0, platform: platform, deploymentTarget: deploymentTarget, organizationName: organizationName) })
        return Project(name: name,
                       organizationName: organizationName,
                       targets: targets)
    }

    // MARK: - Private

    /// Helper function to create a framework target and an associated unit test target
    private static func makeFrameworkTargets(
        name: String,
        platform: Platform,
        deploymentTarget: ProjectDescription.DeploymentTarget,
        organizationName: String
    ) -> [Target] {
        let sources = Target(
            name: name,
            platform: platform,
            product: .framework,
            bundleId: "\(organizationName).\(name)",
            deploymentTarget: deploymentTarget,
            infoPlist: .default,
            sources: ["Targets/\(name)/Sources/**"],
            resources: [],
            dependencies: [])
        let tests = Target(
            name: "\(name)Tests",
            platform: platform,
            product: .unitTests,
            bundleId: "\(organizationName).\(name)Tests",
            deploymentTarget: deploymentTarget,
            infoPlist: .default,
            sources: ["Targets/\(name)/Tests/**"],
            resources: [],
            dependencies: [.target(name: name)])
        return [sources, tests]
    }

    /// Helper function to create the application target and the unit test target.
    private static func makeAppTargets(
        name: String,
        platform: Platform,
        deploymentTarget: ProjectDescription.DeploymentTarget,
        dependencies: [TargetDependency],
        organizationName: String
    ) -> [Target] {
        let platform: Platform = platform
        let customInfoPlist = InfoPlist.file(path: "Config/Mealing-Info.plist")

        let mainTarget = Target(
            name: name,
            platform: platform,
            product: .app,
            bundleId: "\(organizationName).\(name)",
            deploymentTarget: deploymentTarget,
            infoPlist: customInfoPlist,
            sources: ["Targets/\(name)/Sources/**"],
            resources: ["Targets/\(name)/Resources/**"],
            dependencies: dependencies
        )

        let testTarget = Target(
            name: "\(name)Tests",
            platform: platform,
            product: .unitTests,
            bundleId: "\(organizationName).\(name)Tests",
            deploymentTarget: deploymentTarget,
            infoPlist: .default,
            sources: ["Targets/\(name)/Tests/**"],
            dependencies: [
                .target(name: "\(name)")
        ])
        return [mainTarget, testTarget]
    }
}
