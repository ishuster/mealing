
import ProjectDescription

let dependencies = Dependencies(
    carthage: [
        .github(path: "onevcat/Kingfisher", requirement: .upToNext("7.0.0")),
    ],
    platforms: [.iOS]
)
