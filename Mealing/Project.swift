import ProjectDescription
import ProjectDescriptionHelpers

/*
                +-------------+
                |             |
                |     App     | Contains Mealing App target and Mealing unit-test target
                |             |
         +------+-------------+-------+
         |         depends on         |
         |                            |
 +----v-----+                   +-----v-----+
 |          |                   |           |
 |   Kit    |                   |     UI    |   Two independent frameworks to share code and start modularising your app
 |          |                   |           |
 +----------+                   +-----------+

 */

// MARK: - Project

// Creates our project using a helper function defined in ProjectDescriptionHelpers
let project = Project.app(name: "Mealing",
                          platform: .iOS,
                          deploymentTarget: .iOS(targetVersion: "15.0", devices: [.iphone]),
                          externalDependencies: ["Kingfisher"],
                          organizationName: "io.alekscbarragan",
                          additionalTargets: ["MealingKit", "MealingUI", "MealingNetwork"])
