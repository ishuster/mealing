import UIKit
import MealingNetwork

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil
    ) -> Bool {

        #if DEBUG
        MealingNetworkConfiguration.configure(environment: .dev)
        #else
        MealingNetworkConfiguration.configure(environment: .prod)
        #endif

        // Setting up root dependencies
        // root dependencies will be propaganted in a tree graph.
        // that's why we need to inject them on app start up.
        let networkService = MealingNetworkConfiguration.makeNetworkServicing(session: .shared)
        let networkClient = NetworkClient(networkServicing: networkService)

        let categoryRepository = CategoryRepositoryProvider(networkClient: networkClient)
        let mealRepository = MealRepositoryProvider(networkClient: networkClient)

        let categoriesPresenter = CategoriesPresenter(categoryRepository: categoryRepository, mealRepository: mealRepository)

        let viewController = categoriesPresenter.viewController
        let navigationController = UINavigationController(rootViewController: viewController)

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()

        let barAppearance = UINavigationBarAppearance()
        barAppearance.configureWithOpaqueBackground()
        barAppearance.backgroundColor = .systemOrange
        barAppearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        barAppearance.largeTitleTextAttributes = [
            .foregroundColor: UIColor.white,
            .font: UIFont.preferredFont(forTextStyle: .largeTitle)
        ]

        navigationController.navigationBar.tintColor = .white
        navigationController.navigationBar.standardAppearance = barAppearance
        navigationController.navigationBar.scrollEdgeAppearance = barAppearance
        navigationController.navigationItem.largeTitleDisplayMode = .automatic
        navigationController.navigationBar.prefersLargeTitles = true

        return true
    }

}
