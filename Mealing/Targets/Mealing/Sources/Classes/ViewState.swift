
import Foundation

enum ViewState: Hashable {
    case loading
    case refreshing
    case loaded
}
