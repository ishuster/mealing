
import Foundation
import MealingNetwork

struct MealDetailEndpoint: EndpointConvertible {

    var path: String = "/lookup.php"

    var parameters: Parameters? {
        return ["i": mealIdentifier]
    }

    var httpMethod: HTTPMethod = .get

    let mealIdentifier: String

    internal init(mealIdentifier: String) {
        self.mealIdentifier = mealIdentifier
    }
}
