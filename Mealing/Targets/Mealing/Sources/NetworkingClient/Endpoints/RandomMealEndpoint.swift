
import Foundation
import MealingNetwork

struct RandomMealEndpoint: EndpointConvertible {

    var path: String = "/random.php"

    var parameters: Parameters? {
        nil
    }

    var httpMethod: HTTPMethod = .get

    internal init() { }
}
