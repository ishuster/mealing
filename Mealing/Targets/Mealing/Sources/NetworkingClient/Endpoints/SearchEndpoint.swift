
import Foundation
import MealingNetwork

struct SearchEndpoint: EndpointConvertible {

    var path: String = "/search.php"

    var parameters: Parameters? {
        return ["s": mealName]
    }

    var httpMethod: HTTPMethod = .get

    let mealName: String

    internal init(mealName: String) {
        self.mealName = mealName
    }
}
