
import Foundation
import MealingNetwork

struct MealByCategoryEndpoint: EndpointConvertible {
    var path: String = "/filter.php"

    var parameters: Parameters? {
        return ["c": category]
    }

    var httpMethod: HTTPMethod = .get

    let category: String

    init(category: String) {
        self.category = category
    }
}

