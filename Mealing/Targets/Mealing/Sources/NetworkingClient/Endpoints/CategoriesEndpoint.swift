
import Foundation
import MealingNetwork

struct CategoriesEndpoint: EndpointConvertible {
    var path: String { "/categories.php" }

    var parameters: Parameters? { nil }

    var httpMethod: HTTPMethod { .get }
}

