
import Foundation
import MealingNetwork
import MealingKit

typealias MealCategoriesCompletion = (Result<[MealCategory], Error>) -> Void
typealias MealsByCategoryCompletion = (Result<[Meal], Error>) -> Void
typealias MealDetailCompletion = (Result<MealDetail, Error>) -> Void
typealias SearchCompletion = (Result<[MealDetail], Error>) -> Void

protocol NetworkingClient {
    func requestMealCategories(completion: @escaping MealCategoriesCompletion)

    func requestMeals(byCategory category: String, completion: @escaping MealsByCategoryCompletion)

    func requestMealDetail(byMealIdentifier identifier: String, completion: @escaping MealDetailCompletion)

    func requestMealSearch(byName name: String, completion: @escaping SearchCompletion)
    func requestRandomMeal(completion: @escaping MealDetailCompletion)
}

final class NetworkClient {

    let networkServicing: MealingNetworkServicing

    init(networkServicing: MealingNetworkServicing) {
        self.networkServicing = networkServicing
    }
}

extension NetworkClient: NetworkingClient {
    func requestMealCategories(completion: @escaping MealCategoriesCompletion) {
        let endpoint = CategoriesEndpoint()
        request(endpoint: endpoint) { (result: Result<CategoriesResponse, Error>) in
            switch result {
            case .success(let response):
                completion(.success(response.categories))

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func requestMeals(byCategory category: String, completion: @escaping MealsByCategoryCompletion) {
        let endpoint = MealByCategoryEndpoint(category: category)
        request(endpoint: endpoint) { (result: Result<MealsByCategoryResponse, Error>) in
            switch result {
            case .success(let response):
                completion(.success(response.meals))

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func requestMealDetail(byMealIdentifier identifier: String, completion: @escaping MealDetailCompletion) {
        let endpoint = MealDetailEndpoint(mealIdentifier: identifier)
        request(endpoint: endpoint) { (result: Result<MealDetailResponse, Error>) in
            switch result {
            case .success(let response):
                
                guard let mealDetail = response.meals.first else {
                    return
                }
                completion(.success(mealDetail))

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func requestMealSearch(byName name: String, completion: @escaping SearchCompletion) {
        let endpoint = SearchEndpoint(mealName: name)
        request(endpoint: endpoint) { (result: Result<MealDetailResponse, Error>) in
            switch result {
            case .success(let response):
                completion(.success(response.meals))

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func requestRandomMeal(completion: @escaping MealDetailCompletion) {
        let endpoint = RandomMealEndpoint()
        request(endpoint: endpoint) { (result: Result<MealDetailResponse, Error>) in
            switch result {
            case .success(let response):
                guard let mealDetail = response.meals.first else {
                    return
                }
                completion(.success(mealDetail))

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    private func request<Model: Codable>(endpoint: EndpointConvertible, completion: @escaping (Result<Model, Error>) -> Void) {
        networkServicing.request(endpoint: endpoint, completion: completion)
    }
}
