
import Foundation
import MealingKit

struct MealDetailResponse: Codable {
    var meals: [MealDetail]
}
