
import Foundation
import MealingKit

struct CategoriesResponse: Codable {
    var categories: [MealCategory]
}

