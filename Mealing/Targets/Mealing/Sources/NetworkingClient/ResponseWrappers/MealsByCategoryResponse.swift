
import Foundation
import MealingKit

struct MealsByCategoryResponse: Codable {
    var meals: [Meal]
}
