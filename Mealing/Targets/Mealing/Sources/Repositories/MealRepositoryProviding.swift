
import Foundation
import MealingKit

protocol MealRepositoryProviding: AnyObject {
    func requestMeals(byCategoryName identifier: String, completion: @escaping MealsByCategoryCompletion)

    func requestMealDetail(byMealIdentifier identifier: String, completion: @escaping MealDetailCompletion)

    func requestRandomMeal(completion: @escaping MealDetailCompletion)

    func meals(byCategoryName name: String) -> [Meal]?

    func mealDetail(byMealIdentifier identifier: String) -> MealDetail?
}

final class MealRepositoryProvider {
    internal init(networkClient: NetworkingClient) {
        self.networkClient = networkClient
    }

    let networkClient: NetworkingClient

    private var mealsByCategoryCache = [String: [Meal]]()
    private var mealDetailCache = [String: MealDetail]()
    private lazy var queue = DispatchQueue(label: Bundle.main.bundleIdentifier ?? "\(type(of: self))")
}

extension MealRepositoryProvider: MealRepositoryProviding {

    func meals(byCategoryName name: String) -> [Meal]? {
        return queue.sync { [unowned self] in
            return mealsByCategoryCache[name]
        }
    }

    func mealDetail(byMealIdentifier identifier: String) -> MealDetail? {
        return queue.sync {
            return mealDetailCache[identifier]
        }
    }

    func requestMeals(byCategoryName categoryName: String, completion: @escaping MealsByCategoryCompletion) {
        if let cachedMeals = meals(byCategoryName: categoryName), !cachedMeals.isEmpty {
            completion(.success(cachedMeals))
            return
        }

        // If Meals aren't cached. Request them
        networkClient.requestMeals(byCategory: categoryName) { [weak self] (result: Result<[Meal], Error>) in
            guard let self = self else { return }
            switch result {
            case .success(let meal):
                self.queue.async {
                    self.mealsByCategoryCache[categoryName] = meal
                }
                completion(.success(meal))
            case .failure(let failure):
                completion(.failure(failure))
            }
        }
    }

    func requestMealDetail(byMealIdentifier identifier: String, completion: @escaping MealDetailCompletion) {
        // try first memory cache
        if let cachedMealDetail = mealDetailCache[identifier] {
            completion(.success(cachedMealDetail))
            return
        }

        networkClient.requestMealDetail(byMealIdentifier: identifier) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let mealDetail):
                self.queue.async {
                    self.mealDetailCache[identifier] = mealDetail
                }
                completion(.success(mealDetail))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

    func requestRandomMeal(completion: @escaping MealDetailCompletion) {
        networkClient.requestRandomMeal { result in
            switch result {
            case .success(let mealDetail):
                self.queue.async {
                    self.mealDetailCache[mealDetail.idMeal] = mealDetail
                }
                completion(.success(mealDetail))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

}
