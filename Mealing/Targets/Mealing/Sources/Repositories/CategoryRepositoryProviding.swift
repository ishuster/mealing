
import Foundation
import MealingKit

protocol CategoryRepositoryProviding: AnyObject {
    func requestMealCategories(completion: @escaping MealCategoriesCompletion)
    func mealCategory(byIdentifier identifier: String) -> MealCategory?
}

final class CategoryRepositoryProvider {
    internal init(networkClient: NetworkingClient) {
        self.networkClient = networkClient
    }

    let networkClient: NetworkingClient

    private var categoriesCache = [String: MealCategory]()
    private lazy var queue = DispatchQueue(label: Bundle.main.bundleIdentifier ?? "\(type(of: self))")
}

extension CategoryRepositoryProvider: CategoryRepositoryProviding {

    func mealCategory(byIdentifier identifier: String) -> MealCategory? {
        return queue.sync { [unowned self] in
            return categoriesCache[identifier]
        }
    }

    func requestMealCategories(completion: @escaping MealCategoriesCompletion) {
        networkClient.requestMealCategories { [unowned self] result in
            switch result {
            case .success(let mealCategories):
                // Cache in memory the `MealCategory` instances by its id.
                queue.async {
                    mealCategories.forEach { mealCategory in
                        self.categoriesCache[mealCategory.idCategory] = mealCategory
                    }
                }

                completion(.success(mealCategories))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
