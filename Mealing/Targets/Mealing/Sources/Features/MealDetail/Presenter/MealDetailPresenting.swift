
import UIKit
import MealingKit

protocol MealDetailPresenting: AnyObject {

    var viewController: MealDetailViewController { get }

    /// Explicitely tells the presenter that async work has finished
    func didFinishApplyingChanges()

    func viewDidLoad()
}

final class MealDetailPresenter: MealDetailPresenting {
    let mealIdentifier: String
    let mealRepository: MealRepositoryProviding

    weak var delegate: MealDetailPresentingDelegate?

    private var _viewController: MealDetailViewController?

    init(mealIdentifier: String, mealRepository: MealRepositoryProviding) {
        self.mealIdentifier = mealIdentifier
        self.mealRepository = mealRepository
    }
}
// MARK: - Public API -

extension MealDetailPresenter {
    var viewController: MealDetailViewController {
        if let viewController = _viewController {
            return viewController
        }

        let viewController = MealDetailViewController(mealDetailPresenting: self)
        _viewController = viewController
        delegate = _viewController
        return viewController
    }

    func viewDidLoad() {
        delegate?.presenter(self, didEnterState: .loading)
        requestMealDetail(mealIdentifier: mealIdentifier, onQueue: .main)
    }

    func didFinishApplyingChanges() {
        delegate?.presenter(self, didEnterState: .loaded)
    }
}

// MARK: - Private API -

private extension MealDetailPresenter {
    func requestMealDetail(mealIdentifier: String, onQueue: DispatchQueue) {
        mealRepository.requestMealDetail(byMealIdentifier: mealIdentifier) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let mealDetail):
                let viewModel = MealDetailViewModel(mealDetail: mealDetail)
                onQueue.async {
                    self.delegate?.presenter(self, didLoadMealDetailViewModel: viewModel)
                }

            case .failure(let error):
                print(error)
            }
        }
    }
}
