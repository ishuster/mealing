import Foundation

protocol MealDetailPresentingDelegate: AnyObject {
    func presenter(_ presenter: MealDetailPresenting, didEnterState state: ViewState)
    func presenter(_ presenter: MealDetailPresenting, didLoadMealDetailViewModel viewModel: MealDetailViewModeling)
    func presenter(_ presenter: MealDetailPresenting, didFailWithError error: Error)
}
