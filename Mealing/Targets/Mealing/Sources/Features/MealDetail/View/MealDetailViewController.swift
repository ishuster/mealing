
import UIKit
import MealingUI

final class MealDetailViewController: UIViewController {

    let presenter: MealDetailPresenting

    private lazy var contentView: MealDetailView = {
        let view = MealDetailView(delegate: self)
        return view
    }()

    init(mealDetailPresenting: MealDetailPresenting) {
        self.presenter = mealDetailPresenting
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required public init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = contentView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.largeTitleDisplayMode = .never
        presenter.viewDidLoad()
    }

}

// MARK: - MealDetailPresentingDelegate -

extension MealDetailViewController: MealDetailPresentingDelegate {

    func presenter(_ presenter: MealDetailPresenting, didEnterState state: ViewState) {
        switch state {
        case .loading:
            contentView.startActivityIndicator()

        case .refreshing:
            print("refreshing")

        case .loaded:
            contentView.stopActivityIndicator()
        }
    }

    func presenter(_ presenter: MealDetailPresenting, didLoadMealDetailViewModel viewModel: MealDetailViewModeling) {
        title = viewModel.meal
        navigationItem.title = viewModel.meal
        contentView.configure(with: viewModel)
        presenter.didFinishApplyingChanges()
    }

    func presenter(_ presenter: MealDetailPresenting, didFailWithError error: Error) {
        contentView.configure(error: error)
    }
}

extension MealDetailViewController: MealDetailViewDelegate {
    func mealDetailViewDidTapRetry(_ view: MealDetailView) {
        presenter.viewDidLoad()
    }
}
