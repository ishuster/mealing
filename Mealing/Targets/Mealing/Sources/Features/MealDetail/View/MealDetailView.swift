
import MealingUI
import UIKit

protocol MealDetailViewDelegate: AnyObject {
    func mealDetailViewDidTapRetry(_ view: MealDetailView)
}

final class MealDetailView: UIView {

    let scrollableStackView: ScrollableStackView = {
        let stackView = ScrollableStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    let placeholderView: PlaceholderView = {
        let view = PlaceholderView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    weak var delegate: MealDetailViewDelegate?

    init(delegate: MealDetailViewDelegate) {
        self.delegate = delegate
        super.init(frame: .zero)
        setUpViews()
    }

    @available(*, unavailable)
    required public init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Public API -

extension MealDetailView {

    func startActivityIndicator() {
        placeholderView.showActivityIndicator(animated: true)
    }

    func stopActivityIndicator() {
        placeholderView.hide(animated: true)
    }

    func configure(error: Error) {
        let imageConfig = UIImage.SymbolConfiguration(pointSize: 80)
        placeholderView.tintColor = .systemOrange
        placeholderView.show(
            withTitle: NSLocalizedString("Error", comment: ""),
            message: error.localizedDescription,
            image: UIImage(systemName: "xmark.octagon", withConfiguration: imageConfig),
            animated: true)
        placeholderView.configureAction(withActionTitle: "Retry") { [unowned self] in
            self.delegate?.mealDetailViewDidTapRetry(self)
        }
    }

    func configure(with viewModel: MealDetailViewModeling) {

        if !viewModel.thumbUrl.isEmpty {
            let imageView = UIImageView()
            imageView.contentMode = .scaleAspectFill
            imageView.kf.setImage(with: URL(string: viewModel.thumbUrl))
            addView(imageView)
            imageView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.3).isActive = true
            imageView.layer.cornerRadius = 8
            imageView.clipsToBounds = true
        }

        if !viewModel.meal.isEmpty {
            let label = makeLabel(text: viewModel.meal, font: .preferredFont(forTextStyle: .title1))
            addView(label)
        }

        if !viewModel.category.isEmpty {

            // using a `UIButton` instance to avoid extra customization of a `UILabel`
            let dummyButton = UIButton()

            dummyButton.layer.borderWidth = 1.0 / UIScreen.main.scale
            dummyButton.layer.borderColor = UIColor.systemOrange.cgColor

            dummyButton.setTitle(viewModel.category, for: .normal)
            dummyButton.setTitleColor(.white, for: .normal)
            dummyButton.backgroundColor = .systemOrange
            dummyButton.isUserInteractionEnabled = false
            dummyButton.titleLabel?.font = .preferredFont(forTextStyle: .footnote)

            let container = UIView()
            container.addSubview(dummyButton)
            dummyButton.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                dummyButton.leadingAnchor.constraint(equalTo: container.leadingAnchor),
                dummyButton.topAnchor.constraint(equalTo: container.topAnchor),
                dummyButton.bottomAnchor.constraint(equalTo: container.bottomAnchor)
            ])
            container.layoutIfNeeded()
            // compute the size of the button with current text to change the corner radious
            // and make it look as a semi circle
            let size = dummyButton.systemLayoutSizeFitting(UIView.layoutFittingExpandedSize, withHorizontalFittingPriority: .fittingSizeLevel, verticalFittingPriority: .fittingSizeLevel)
            dummyButton.layer.cornerRadius = size.height / 2.0
            // set a width constraint with a padding of 20 so left is 10 and right is 10
            dummyButton.widthAnchor.constraint(equalToConstant: size.width + 20).isActive = true
            addView(container)
        }

        if !viewModel.ingredients.isEmpty {
            addTitleLabel(text: "Ingredients", comment: "Ingredients meal detail title")

            let ingredientsStackViews: [UIStackView] = viewModel.ingredients.compactMap({ (ingredient: String, measure: String) in
                if ingredient.isEmpty && measure.isEmpty {
                    return nil
                }

                let font: UIFont = .preferredFont(forTextStyle: .footnote)

                let ingredientLabel = makeLabel(text: ingredient, font: font)
                let measureLabel = makeLabel(text: measure, font: font)

                let stackView = UIStackView(arrangedSubviews: [ingredientLabel, measureLabel])
                stackView.axis = .horizontal
                stackView.distribution = .fillEqually
                stackView.translatesAutoresizingMaskIntoConstraints = false

                return stackView
            })

            let stackView = UIStackView(arrangedSubviews: ingredientsStackViews)
            stackView.axis = .vertical
            stackView.translatesAutoresizingMaskIntoConstraints = false
            addView(stackView)
        }

        if !viewModel.instructions.isEmpty {
            addTitleLabel(text: "Instructions", comment: "Instructions to prepare meal")

            let instructionsLabel = makeLabel(text: viewModel.instructions, font: .preferredFont(forTextStyle: .footnote))
            addView(instructionsLabel)
        }
    }

}

// MARK: - Private API -

private extension MealDetailView {

    /// Add a title with its separator
    /// - Parameters:
    ///   - text: Localized string key. It uses `NSLocalizedString` to locale passed string.
    ///   - comment: Comment to identify localized string.
    func addTitleLabel(text: String, comment: String) {
        let separatorView = UIView()
        separatorView.backgroundColor = .systemOrange
        addView(separatorView)
        separatorView.heightAnchor.constraint(equalToConstant: 1 / UIScreen.main.scale).isActive = true

        let titleLabel = UILabel()
        titleLabel.font = .preferredFont(forTextStyle: .title3)
        titleLabel.text = NSLocalizedString(text, comment: "Instructions to prepare meal")
        addView(titleLabel)

        let separatorBottomView = UIView()
        separatorBottomView.backgroundColor = .systemOrange
        addView(separatorBottomView)
        separatorBottomView.heightAnchor.constraint(equalToConstant: 1 / UIScreen.main.scale).isActive = true
    }

    func makeLabel(text: String, comment: String = "", font: UIFont) -> UILabel {
        let label = UILabel()
        label.font = font
        label.text = NSLocalizedString(text, comment: comment)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }

    func addView(_ view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        scrollableStackView.addView(view)
    }

    func setUpViews() {
        setUpSelf()
        setUpViewHierarchy()
        setUpConstraints()
    }

    func setUpSelf() {
        backgroundColor = .systemBackground
    }

    func setUpViewHierarchy() {
        addSubview(scrollableStackView)
        addSubview(placeholderView)
    }

    func setUpConstraints() {
        let safeGuide = safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            scrollableStackView.leadingAnchor.constraint(equalTo: safeGuide.leadingAnchor),
            scrollableStackView.topAnchor.constraint(equalTo: safeGuide.topAnchor),
            scrollableStackView.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor),
            scrollableStackView.bottomAnchor.constraint(equalTo: safeGuide.bottomAnchor),
            // placeholderView
            placeholderView.leadingAnchor.constraint(equalTo: safeGuide.leadingAnchor),
            placeholderView.topAnchor.constraint(equalTo: safeGuide.topAnchor),
            placeholderView.trailingAnchor.constraint(equalTo: safeGuide.trailingAnchor),
            placeholderView.bottomAnchor.constraint(equalTo: safeGuide.bottomAnchor),
        ])
    }
}
