
import Foundation
import MealingKit

protocol MealDetailViewModeling {
    var identifier: String { get }
    var category: String { get }
    var meal: String { get }
    var thumbUrl: String { get }
    var instructions: String { get }
    var ingredients: [(ingredient: String, measure: String)] { get }
}

struct MealDetailViewModel: MealDetailViewModeling {

    var identifier: String
    var category: String
    var meal: String
    var thumbUrl: String
    var instructions: String
    var ingredients: [(ingredient: String, measure: String)]

    let mealDetail: MealDetail

    init(mealDetail: MealDetail) {
        self.mealDetail = mealDetail
        identifier = mealDetail.idMeal
        category = mealDetail.strCategory
        meal = mealDetail.strMeal
        thumbUrl = mealDetail.strMealThumb
        instructions = mealDetail.strInstructions

        var ingredients: [(ingredient: String, measure: String)] = []
        let measures: [String] = mealDetail.measures

        for (index, ingredient) in mealDetail.ingredients.enumerated() {
            let newIngredient: (ingredient: String, measure: String)
            let measure = measures[index]

            newIngredient = (
                ingredient.trimmingCharacters(in: .whitespacesAndNewlines),
                measure.trimmingCharacters(in: .whitespacesAndNewlines)
            )

            ingredients.append(newIngredient)
        }
        self.ingredients = ingredients
    }
}
