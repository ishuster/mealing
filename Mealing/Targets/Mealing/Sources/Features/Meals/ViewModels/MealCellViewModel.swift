
import Foundation
import MealingKit

struct MealCellViewModel: Hashable {
    let meal: Meal

    var identifier: String {
        return meal.idMeal
    }
}

extension MealCellViewModel: MealCategoryCellModeling {
    var title: String {
        return meal.strMeal
    }

    var subtitle: String {
        return meal.idMeal
    }

    var imageUrl: String {
        return meal.strMealThumb
    }
}
