
import Foundation
import MealingKit

typealias MealsDataSourceSnapshot = MealsDataSource.Snapshot
extension MealsDataSourceSnapshot {
    static func snapshot(meals: [Meal]) -> MealsDataSourceSnapshot {
        var snapshot = Self.init()
        snapshot.appendMealCategoriesSection(meals: meals)
        return snapshot
    }
}

private extension MealsDataSourceSnapshot {
    mutating func appendMealCategoriesSection(meals: [Meal]) {
        appendSections([.meals])
        let viewModels: [MealItem] = meals
            .map(MealCellViewModel.init)
            .map { viewModel in MealItem.meal(viewModel) }
        appendItems(viewModels, toSection: .meals)
    }
}
