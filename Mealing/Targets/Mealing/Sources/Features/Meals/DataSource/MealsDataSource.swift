import UIKit

final class MealsDataSource: UITableViewDiffableDataSource<MealCellSection, MealItem> {
    typealias Snapshot = NSDiffableDataSourceSnapshot<MealCellSection, MealItem>

    weak var categoriesView: MealsView?
    weak var delegate: MealsViewDelegate?

    init(tableView: UITableView, categoriesView: MealsView, delegate: MealsViewDelegate?) {
        self.categoriesView = categoriesView
        self.delegate = delegate

        tableView.register(cellType: MealOverviewTableViewCell.self)

        super.init(tableView: tableView) { tableView, indexPath, itemIdentifier in
            let cell = tableView.dequeueCell(for: indexPath) as MealOverviewTableViewCell
            switch itemIdentifier {
            case .meal(let viewModel):
                cell.configure(with: viewModel)
                /// Take adventage of the capture list to capture curernt values in the actionHandler
                cell.actionHandler = { action in
                    delegate?.mealsView(categoriesView, didSelectMealIdentifier: viewModel.identifier)
                }
            }
            return cell
        }

        defaultRowAnimation = .fade
    }
}

// MARK: - UITableViewDelegate -

extension MealsDataSource: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let cell = tableView.cellForRow(at: indexPath) as? MealOverviewTableViewCell else {
            return
        }

        cell.actionHandler?(.didSelectCell)
    }
}
