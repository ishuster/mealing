
import Foundation

enum MealItem: Hashable {
    case meal(_ mealCellViewModel: MealCellViewModel)
}
