
import Foundation
import UIKit

protocol MealsPresenting: AnyObject {
    var viewController: MealsViewController { get }
    var title: String { get }
    func viewDidLoad()
    func didFinishApplyingChanges()

    func makeMealDetailPresenter(byMealIdentifier identifier: String) -> MealDetailPresenting
}

protocol MealsPresentingDelegate: AnyObject {
    func mealsPresenter(_ presenter: MealsPresenting, didEnterState state: ViewState)
    func mealsPresenter(_ presenter: MealsPresenting, didLoadCategoriesViewModel viewModel: MealsView.ViewModel)
    func mealsPresenter(_ presenter: MealsPresenting, didFailWithError error: Error)
}

final class MealsPresenter {

    weak var delegate: MealsPresentingDelegate?

    let mealCategoryIdentifier: String
    let categoryName: String

    let mealRepository: MealRepositoryProviding
    let categoryRepository: CategoryRepositoryProviding

    init(mealCategoryIdentifier: String, categoryName: String, mealRepository: MealRepositoryProviding, categoryRepository: CategoryRepositoryProviding) {
        self.mealCategoryIdentifier = mealCategoryIdentifier
        self.categoryName = categoryName
        self.mealRepository = mealRepository
        self.categoryRepository = categoryRepository
    }

    /// Reference to view controller that it is tightly coupled to this presenter.
    private var _viewController: MealsViewController?
}

extension MealsPresenter: MealsPresenting {

    var title: String {
        guard let category = categoryRepository.mealCategory(byIdentifier: mealCategoryIdentifier) else {
            return ""
        }
        return category.strCategory
    }

    var viewController: MealsViewController {
        if let viewController = _viewController {
            return viewController
        }

        let viewController = MealsViewController(mealsPresenting: self)
        _viewController = viewController
        delegate = viewController
        return viewController
    }

    func viewDidLoad() {
        delegate?.mealsPresenter(self, didEnterState: .loading)
        requestMeals(byCategoryName: categoryName, onQueue: .main)
    }

    func didFinishApplyingChanges() {
        delegate?.mealsPresenter(self, didEnterState: .loaded)
    }

    func makeMealDetailPresenter(byMealIdentifier identifier: String) -> MealDetailPresenting {
        let presenter = MealDetailPresenter(mealIdentifier: identifier, mealRepository: mealRepository)
        return presenter
    }

}

// MARK: - Private API -

private extension MealsPresenter {
    func requestMeals(byCategoryName name: String, onQueue queue: DispatchQueue) {
        mealRepository.requestMeals(byCategoryName: name) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let meals):
                let snapshot = MealsDataSourceSnapshot.snapshot(meals: meals)
                queue.async {
                    self.delegate?.mealsPresenter(self, didLoadCategoriesViewModel: .init(snapshot: snapshot))
                }

            case .failure(let error):
                queue.async {
                    self.delegate?.mealsPresenter(self, didFailWithError: error)
                }
            }
        }
    }
}
