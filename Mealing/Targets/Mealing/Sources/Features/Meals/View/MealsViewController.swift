
import UIKit

final class MealsViewController: UIViewController {
    private lazy var contentView = MealsView(delegate: self)

    let presenter: MealsPresenting
    init(mealsPresenting: MealsPresenting) {
        self.presenter = mealsPresenting
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = contentView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let title = presenter.title
        self.title = title
        navigationItem.title = title
        presenter.viewDidLoad()
    }
}

// MARK: - MealsViewDelegate -

extension MealsViewController: MealsViewDelegate {
    func mealsView(_ view: MealsView, didSelectMealIdentifier identifier: String) {
        let mealDetailPresenter = presenter.makeMealDetailPresenter(byMealIdentifier: identifier)
        navigationController?.pushViewController(mealDetailPresenter.viewController, animated: true)
    }

    func mealsViewDidSelectRetry(_ view: MealsView) {
        presenter.viewDidLoad()
    }
}

// MARK: - MealsPresentingDelegate -

extension MealsViewController: MealsPresentingDelegate {
    func mealsPresenter(_ presenter: MealsPresenting, didEnterState state: ViewState) {
        switch state {
        case .loading:
            contentView.startLoading()
        case .refreshing:
            print("show refreshing indicator")
        case .loaded:
            contentView.hideLoader(animated: true)
        }
    }

    func mealsPresenter(_ presenter: MealsPresenting, didLoadCategoriesViewModel viewModel: MealsView.ViewModel) {
        contentView.apply(viewModel: viewModel) {
            presenter.didFinishApplyingChanges()
        }
    }

    func mealsPresenter(_ presenter: MealsPresenting, didFailWithError error: Error) {
        contentView.configure(error: error)
    }
}
