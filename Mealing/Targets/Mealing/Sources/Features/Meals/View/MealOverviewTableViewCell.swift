//
//  MealOverviewTableViewCell.swift
//  Mealing
//
//  Created by shuster on 11/03/22.
//  Copyright © 2022 io.alekscbarragan. All rights reserved.
//

import Kingfisher
import UIKit

final class MealOverviewTableViewCell: UITableViewCell {

    enum Action {
        case didSelectCell
    }

    var actionHandler: ((Action) -> Void)?

    let mealImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = .unit
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = .preferredFont(forTextStyle: .title3)
        return label
    }()

    private var leadingTitleConstraint: NSLayoutConstraint?
    private var trailingTitleConstraint: NSLayoutConstraint?
    private var topTitleConstraint: NSLayoutConstraint?
    private var bottomTitleConstraint: NSLayoutConstraint?

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        setUpView()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        let margins = directionalLayoutMargins
        leadingTitleConstraint?.constant = margins.leading
        trailingTitleConstraint?.constant = -margins.trailing
    }
}

// MARK: - Public API -

extension MealOverviewTableViewCell {
    func configure(with viewMode: MealCategoryCellModeling) {
        titleLabel.text = viewMode.title
        mealImageView.kf.indicatorType = .activity
        mealImageView.kf.setImage(with: URL(string: viewMode.imageUrl))
    }
}

// MARK: - Private API -

private extension MealOverviewTableViewCell {
    func setUpView() {
        setUpViewHierarchy()
        setUpAutolayout()
        mealImageView.backgroundColor = .systemGray2
        backgroundColor = .clear
    }

    func setUpViewHierarchy() {
        contentView.addSubview(mealImageView)
        contentView.addSubview(titleLabel)
    }

    func setUpAutolayout() {
        let leadingConstraint = titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor)
        let topConstraint = titleLabel.topAnchor.constraint(equalTo: mealImageView.bottomAnchor, constant: .unit)
        let trailingConstraint = titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
        let bottomConstraint = titleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.unit)
        let safeGuide = contentView.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            mealImageView.widthAnchor.constraint(equalTo: contentView.widthAnchor),
            mealImageView.heightAnchor.constraint(equalTo: mealImageView.widthAnchor, multiplier: 0.5),
            mealImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            mealImageView.topAnchor.constraint(equalTo: safeGuide.topAnchor, constant: directionalLayoutMargins.top),
            mealImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            // title label
            leadingConstraint,
            topConstraint,
            trailingConstraint,
            bottomConstraint
        ])

        leadingTitleConstraint = leadingConstraint
        trailingTitleConstraint = trailingConstraint
        topTitleConstraint = topConstraint
        bottomTitleConstraint = bottomConstraint
    }
}

// MARK: - Constants -

private extension CGFloat {
    static var unit: CGFloat { 8 }
}
