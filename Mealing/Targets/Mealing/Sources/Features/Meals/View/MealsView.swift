
import UIKit
import MealingKit
import MealingUI

protocol MealsViewDelegate: AnyObject {
    func mealsView(_ view: MealsView, didSelectMealIdentifier identifier: String)
    func mealsViewDidSelectRetry(_ view: MealsView)
}

final class MealsView: UIView {

    let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()

    let placeholderView: PlaceholderView = {
        let view = PlaceholderView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    weak var delegate: MealsViewDelegate?

    private lazy var dataSource: MealsDataSource = .init(
        tableView: tableView,
        categoriesView: self,
        delegate: delegate
    )

    init(delegate: MealsViewDelegate) {
        self.delegate = delegate
        super.init(frame: .zero)
        setUpView()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Public API -
extension MealsView {
    struct ViewModel {
        var snapshot: MealsDataSourceSnapshot
    }

    func startLoading(animated: Bool = true) {
        placeholderView.showActivityIndicator(animated: animated)
    }

    func hideLoader(animated: Bool = true) {
        placeholderView.hide(animated: animated)
    }

    func apply(viewModel: ViewModel, animatingDifferences: Bool = true, completion: (() -> Void)? = nil) {
        dataSource.apply(viewModel.snapshot, animatingDifferences: animatingDifferences, completion: completion)
    }

    func configure(error: Error) {
        let imageConfig = UIImage.SymbolConfiguration(pointSize: 80)
        placeholderView.tintColor = .systemOrange
        placeholderView.show(
            withTitle: NSLocalizedString("Error", comment: ""),
            message: error.localizedDescription,
            image: UIImage(systemName: "exclamationmark.icloud", withConfiguration: imageConfig),
            animated: true)
        placeholderView.configureAction(withActionTitle: NSLocalizedString("Retry", comment: "")) { [unowned self] in
            self.delegate?.mealsViewDidSelectRetry(self)
        }
    }
}

// MARK: - Private API -
private extension MealsView {
    func setUpView() {
        setUpSubviews()
        setUpTableView()
        tableView.separatorColor = .systemOrange
    }

    func setUpTableView() {
        tableView.delegate = dataSource
    }

    func setUpSubviews() {
        setUpViewHierarchy()
        setUpConstraints()
    }

    func setUpViewHierarchy() {
        addSubview(tableView)
        addSubview(placeholderView)
    }

    func setUpConstraints() {
        let guide = safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: guide.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: guide.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: guide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: guide.bottomAnchor),
            // placeholderView
            placeholderView.leadingAnchor.constraint(equalTo: guide.leadingAnchor),
            placeholderView.topAnchor.constraint(equalTo: guide.topAnchor),
            placeholderView.trailingAnchor.constraint(equalTo: guide.trailingAnchor),
            placeholderView.bottomAnchor.constraint(equalTo: guide.bottomAnchor)
        ])
    }
}

