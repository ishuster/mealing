
import Foundation
import MealingKit

struct MealCategoryCellViewModel: Hashable {
    let mealCategory: MealCategory
}

extension MealCategoryCellViewModel: MealCategoryCellModeling {
    var identifier: String { mealCategory.idCategory }
    var title: String { mealCategory.strCategory }
    var subtitle: String { mealCategory.strCategoryDescription }
    var imageUrl: String { mealCategory.strCategoryThumb }
}
