
import Foundation

protocol MealCategoryCellModeling {
    var title: String { get }
    var subtitle: String { get }
    var imageUrl: String { get }
}
