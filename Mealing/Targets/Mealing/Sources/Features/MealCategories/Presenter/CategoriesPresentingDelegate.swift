
import Foundation

protocol CategoriesPresentingDelegate: AnyObject {
    func categoriesPresenter(_ presenter: CategoriesPresenting, didEnterState state: ViewState)
    func categoriesPresenter(_ presenter: CategoriesPresenting, didLoadCategoriesViewModel viewModel: CategoriesView.ViewModel)
    func categoriesPresenter(_ presenter: CategoriesPresenting, didLoadMealDetailViewModel viewModel: MealDetailViewModeling)
    func categoriesPresenter(_ presenter: CategoriesPresenting, didFailWithError error: Error)
}
