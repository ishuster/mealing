
import Foundation

protocol CategoriesPresenting: AnyObject {
    var viewController: CategoriesViewController { get }
    var title: String { get }
    func viewDidLoad()
    func didFinishApplyingChanges()

    func requestRandomMealIfAvailable()

    func makePresenterForMealsByCategory(identifier: String, categoryName: String) -> MealsPresenting
    func makeMealDetailPresenter(byMealIdentifier identifier: String) -> MealDetailPresenting
}

final class CategoriesPresenter {

    weak var delegate: CategoriesPresentingDelegate?

    let categoryRepository: CategoryRepositoryProviding
    let mealRepository: MealRepositoryProviding

    init(categoryRepository: CategoryRepositoryProviding, mealRepository: MealRepositoryProviding) {
        self.categoryRepository = categoryRepository
        self.mealRepository = mealRepository
    }

    private var _viewController: CategoriesViewController?
}

extension CategoriesPresenter: CategoriesPresenting {
    var title: String {
        return NSLocalizedString("Meal categories", comment: "navigation item title")
    }

    var viewController: CategoriesViewController {
        if let viewController = _viewController {
            return viewController
        }

        let viewController = CategoriesViewController(categoriesPresenting: self)
        _viewController = viewController
        delegate = viewController
        return viewController
    }

    func viewDidLoad() {
        delegate?.categoriesPresenter(self, didEnterState: .loading)
        requestMealCategories(onQueue: .main)
    }

    func didFinishApplyingChanges() {
        delegate?.categoriesPresenter(self, didEnterState: .loaded)
    }

    func makePresenterForMealsByCategory(identifier: String, categoryName: String) -> MealsPresenting {
        let mealsPresenter = MealsPresenter(
            mealCategoryIdentifier: identifier,
            categoryName: categoryName,
            mealRepository: mealRepository,
            categoryRepository: categoryRepository
        )
        return mealsPresenter
    }

    func makeMealDetailPresenter(byMealIdentifier identifier: String) -> MealDetailPresenting {
        let mealDetailPresenter = MealDetailPresenter(mealIdentifier: identifier, mealRepository: mealRepository)
        return mealDetailPresenter
    }

    func requestRandomMealIfAvailable() {
        requestRandomMeal(onQueue: .main)
    }
}

// MARK: - Private API -

enum CustomError: Swift.Error {
    case testError
}

private extension CategoriesPresenter {

    /// Requests `[MealCategory]` to categegory repository
    /// - Parameter queue: `DispatchQueue` you want the response to be handled.
    func requestMealCategories(onQueue queue: DispatchQueue) {
        if Bool.random() {
            DispatchQueue.global().asyncAfter(deadline: .now() + 2) {
                queue.async {
                    self.delegate?.categoriesPresenter(self, didFailWithError: CustomError.testError)
                }
            }
            return
        }
        categoryRepository.requestMealCategories { [weak delegate, weak self] result in
            guard let delegate = delegate, let self = self else { return }
            switch result {
            case .success(let mealCategories):
                let snapshot = CategoriesDataSourceSnapshot.snapshot(mealCategories: mealCategories)
                queue.async {
                    delegate.categoriesPresenter(self, didLoadCategoriesViewModel: .init(snapshot: snapshot))
                }

            case .failure(let error):
                queue.async {
                    delegate.categoriesPresenter(self, didFailWithError: error)
                }
            }
        }
    }

    func requestRandomMeal(onQueue: DispatchQueue) {
        mealRepository.requestRandomMeal { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let mealDetail):
                let viewModel = MealDetailViewModel(mealDetail: mealDetail)
                onQueue.async {
                    self.delegate?.categoriesPresenter(self, didLoadMealDetailViewModel: viewModel)
                }

            case .failure(let error):
                onQueue.async {
                    self.delegate?.categoriesPresenter(self, didFailWithError: error)
                }
            }
        }
    }
}
