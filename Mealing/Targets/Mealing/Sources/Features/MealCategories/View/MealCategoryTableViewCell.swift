
import Kingfisher
import UIKit

final class MealCategoryTableViewCell: UITableViewCell {

    enum Action {
        case didSelectCell
    }

    var actionHandler: ((Action) -> Void)?

    let mealImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = .unit
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.font = .preferredFont(forTextStyle: .title2)
        return label
    }()

    let subtitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.font = .preferredFont(forTextStyle: .body)
        return label
    }()

    let chevronImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = .unit
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = .init(systemName: "chevron.forward")
        imageView.tintColor = .systemOrange
        return imageView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        setUpView()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        mealImageView.layer.cornerRadius = mealImageView.frame.height / 2.0
    }
}

// MARK: - Public API -

extension MealCategoryTableViewCell {
    func configure(with viewModel: MealCategoryCellModeling) {
        titleLabel.text = viewModel.title
        subtitleLabel.text = viewModel.subtitle
        mealImageView.kf.indicatorType = .activity
        mealImageView.kf.setImage(with: URL(string: viewModel.imageUrl), options: [])
    }
}

// MARK: - Private API -

private extension MealCategoryTableViewCell {
    func setUpView() {
        setUpViewHierarchy()
        setUpAutolayout()
    }

    func setUpViewHierarchy() {
        contentView.addSubview(mealImageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(chevronImageView)
    }

    func setUpAutolayout() {
        let margins = directionalLayoutMargins
        NSLayoutConstraint.activate([
            mealImageView.widthAnchor.constraint(equalToConstant: .width),
            mealImageView.heightAnchor.constraint(equalTo: mealImageView.widthAnchor),
            mealImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: margins.leading),
            mealImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: .unit),
            // chevron
            chevronImageView.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
            chevronImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -margins.trailing),
            // title
            titleLabel.leadingAnchor.constraint(equalTo: mealImageView.trailingAnchor, constant: .unit),
            titleLabel.trailingAnchor.constraint(equalTo: chevronImageView.leadingAnchor, constant: -margins.trailing),
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: margins.top),
            titleLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: .width),
            // subtitle
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: margins.top),
            subtitleLabel.leadingAnchor.constraint(equalTo: mealImageView.leadingAnchor),
            subtitleLabel.trailingAnchor.constraint(equalTo: chevronImageView.trailingAnchor),
            subtitleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -.unit)
            //
        ])
    }
}

// MARK: - Constants -

private extension CGFloat {
    static var width: CGFloat { 44 }
    static var unit: CGFloat { 8 }
}
