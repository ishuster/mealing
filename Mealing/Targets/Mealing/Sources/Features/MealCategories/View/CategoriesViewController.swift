
import UIKit

final class CategoriesViewController: UIViewController {

    let presenter: CategoriesPresenting

    /// Content view of view controller. UI actions are binded by the `delegate` (`CategoriesViewDelegate`)
    private lazy var contentView = CategoriesView(delegate: self)

    init(categoriesPresenting: CategoriesPresenting) {
        self.presenter = categoriesPresenting
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        view = contentView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = presenter.title
        navigationItem.title = presenter.title
        presenter.viewDidLoad()
        presenter.requestRandomMealIfAvailable()
    }

}

// MARK: - CategoriesViewDelegate -

extension CategoriesViewController: CategoriesViewDelegate {
    func categoriesView(_ view: CategoriesView, didSelectMealCategoryIdentifier identifier: String, categoryName: String) {
        let mealsPresenter = presenter.makePresenterForMealsByCategory(
            identifier: identifier,
            categoryName: categoryName
        )
        navigationController?.pushViewController(mealsPresenter.viewController, animated: true)
    }

    func categoriesViewDidTapRetry(_ view: CategoriesView) {
        presenter.viewDidLoad()
    }
}

extension CategoriesViewController: CategoriesPresentingDelegate {
    func categoriesPresenter(_ presenter: CategoriesPresenting, didEnterState state: ViewState) {
        switch state {
        case .loading:
            print("show activity indicator")
            contentView.startLoading()

        case .refreshing:
            print("show table activity indicator")

        case .loaded:
            contentView.hideLoader(animated: true)
        }
    }

    func categoriesPresenter(_ presenter: CategoriesPresenting, didLoadCategoriesViewModel viewModel: CategoriesView.ViewModel) {
        contentView.apply(viewModel: viewModel) {
            presenter.didFinishApplyingChanges()
        }
    }

    func categoriesPresenter(_ presenter: CategoriesPresenting, didFailWithError error: Error) {
        print("did fail with error: \(error)")
        contentView.configure(error: error)
    }

    func categoriesPresenter(_ presenter: CategoriesPresenting, didLoadMealDetailViewModel viewModel: MealDetailViewModeling) {
        let title = NSLocalizedString("Random meal available", comment: "")
        let message = NSLocalizedString("Do you want to see the random meal?", comment: "")
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let okayAction = UIAlertAction(title: NSLocalizedString("Show", comment: ""), style: .default) { [unowned self] _ in
            let mealDetailPresenter = presenter.makeMealDetailPresenter(byMealIdentifier: viewModel.identifier)
            self.navigationController?.pushViewController(mealDetailPresenter.viewController, animated: true)
        }

        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { _ in }

        alertController.addAction(cancelAction)
        alertController.addAction(okayAction)

        present(alertController, animated: true)
    }
}
