
import UIKit
import MealingKit
import MealingUI

protocol CategoriesViewDelegate: AnyObject {
    func categoriesView(_ view: CategoriesView, didSelectMealCategoryIdentifier identifier: String, categoryName: String)
    func categoriesViewDidTapRetry(_ view: CategoriesView)
}

final class CategoriesView: UIView {

    let tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()

    let placeholderView: PlaceholderView = {
        let view = PlaceholderView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    weak var delegate: CategoriesViewDelegate?

    private lazy var dataSource: CategoriesDataSource = .init(
        tableView: tableView,
        categoriesView: self,
        delegate: delegate
    )

    init(delegate: CategoriesViewDelegate) {
        self.delegate = delegate
        super.init(frame: .zero)
        setUpView()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Public API -
extension CategoriesView {
    func startLoading(animated: Bool = true) {
        placeholderView.showActivityIndicator(animated: animated)
    }

    func hideLoader(animated: Bool = true) {
        placeholderView.hide(animated: animated)
    }

    func apply(viewModel: ViewModel, animatingDifferences: Bool = true, completion: (() -> Void)? = nil) {
        dataSource.apply(viewModel.snapshot, animatingDifferences: animatingDifferences, completion: completion)
    }

    func configure(error: Error) {
        let imageConfig = UIImage.SymbolConfiguration(pointSize: 80)
        placeholderView.tintColor = .systemOrange
        placeholderView.show(
            withTitle: NSLocalizedString("Error", comment: ""),
            message: error.localizedDescription,
            image: UIImage(systemName: "xmark.octagon", withConfiguration: imageConfig),
            animated: true)
        placeholderView.configureAction(withActionTitle: "Retry") { [unowned self] in
            self.delegate?.categoriesViewDidTapRetry(self)
        }
    }
}

// MARK: - Private API -
private extension CategoriesView {
    func setUpView() {
        setUpSubviews()
        setUpTableView()
        tableView.separatorColor = .systemOrange
        backgroundColor = tableView.backgroundColor
    }

    func setUpTableView() {
        tableView.delegate = dataSource
    }

    func setUpSubviews() {
        setUpViewHierarchy()
        setUpConstraints()
    }

    func setUpViewHierarchy() {
        addSubview(tableView)
        addSubview(placeholderView)
    }

    func setUpConstraints() {
        let guide = safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: guide.leadingAnchor),
            tableView.topAnchor.constraint(equalTo: guide.topAnchor),
            tableView.trailingAnchor.constraint(equalTo: guide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: guide.bottomAnchor),
            // placeholderView
            placeholderView.leadingAnchor.constraint(equalTo: guide.leadingAnchor),
            placeholderView.topAnchor.constraint(equalTo: guide.topAnchor),
            placeholderView.trailingAnchor.constraint(equalTo: guide.trailingAnchor),
            placeholderView.bottomAnchor.constraint(equalTo: guide.bottomAnchor)
        ])
    }
}
