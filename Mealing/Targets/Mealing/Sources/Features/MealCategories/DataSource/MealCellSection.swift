
import Foundation

enum MealCellSection: Hashable {
    case categories
    case meals
}
