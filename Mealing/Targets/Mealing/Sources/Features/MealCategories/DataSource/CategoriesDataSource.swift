
import UIKit

final class CategoriesDataSource: UITableViewDiffableDataSource<MealCellSection, MealCellItem> {
    typealias Snapshot = NSDiffableDataSourceSnapshot<MealCellSection, MealCellItem>

    weak var categoriesView: CategoriesView?
    weak var delegate: CategoriesViewDelegate?

    init(tableView: UITableView, categoriesView: CategoriesView, delegate: CategoriesViewDelegate?) {
        self.categoriesView = categoriesView
        self.delegate = delegate

        tableView.register(cellType: MealCategoryTableViewCell.self)

        super.init(tableView: tableView) { tableView, indexPath, itemIdentifier in
            let cell = tableView.dequeueCell(for: indexPath) as MealCategoryTableViewCell
            switch itemIdentifier {
            case .category(let viewModel):
                cell.configure(with: viewModel)
                /// Take adventage of the capture list to capture curernt values in the actionHandler
                cell.actionHandler = { action in
                    delegate?.categoriesView(categoriesView, didSelectMealCategoryIdentifier: viewModel.identifier, categoryName: viewModel.title)
                }
            }
            return cell
        }

        defaultRowAnimation = .fade
    }
}

// MARK: - UITableViewDelegate -

extension CategoriesDataSource: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let cell = tableView.cellForRow(at: indexPath) as? MealCategoryTableViewCell else {
            return
        }

        cell.actionHandler?(.didSelectCell)
    }
}
