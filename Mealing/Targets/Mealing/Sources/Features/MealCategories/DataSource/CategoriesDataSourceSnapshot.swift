
import Foundation
import MealingKit

typealias CategoriesDataSourceSnapshot = CategoriesDataSource.Snapshot
extension CategoriesDataSourceSnapshot {
    static func snapshot(mealCategories: [MealCategory]) -> CategoriesDataSource.Snapshot {
        var snapshot = Self.init()
        snapshot.appendMealCategoriesSection(mealCategories: mealCategories)
        return snapshot
    }
}

private extension CategoriesDataSourceSnapshot {
    mutating func appendMealCategoriesSection(mealCategories: [MealCategory]) {
        appendSections([.categories])
        let viewModels: [MealCellItem] = mealCategories
            .map(MealCategoryCellViewModel.init)
            .map { viewModel in MealCellItem.category(viewModel) }
        appendItems(viewModels, toSection: .categories)
    }
}
