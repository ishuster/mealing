
import Foundation

enum MealCellItem: Hashable {
    case category(_ mealCategoryCellViewModel: MealCategoryCellViewModel)
}
