
import UIKit

extension UITableViewCell: ReusableView {}
extension UITableViewHeaderFooterView: ReusableView {}

public extension UITableView {
    func register(cellType type: UITableViewCell.Type) {
        register(type, forCellReuseIdentifier: type.reuseIdentifier)
    }

    func register(headerFooterViewType type: UITableViewHeaderFooterView.Type) {
        register(type, forHeaderFooterViewReuseIdentifier: type.reuseIdentifier)
    }

    func dequeueCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("You need to register cell of type `\(T.reuseIdentifier)`")
        }

        return cell
    }

    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>() -> T {
        guard let headerFooterView = dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier) as? T else {
            fatalError("You must register the reusable view of type `\(T.reuseIdentifier)`")
        }

        return headerFooterView
    }
}
