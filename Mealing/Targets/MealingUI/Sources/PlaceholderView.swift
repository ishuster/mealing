
import UIKit

public final class PlaceholderView: UIView {

    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        return imageView
    }()

    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = .preferredFont(forTextStyle: .title1)
        return label
    }()

    let messageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = .preferredFont(forTextStyle: .body)
        return label
    }()

    let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .large)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.hidesWhenStopped = true
        indicator.color = .systemOrange
        indicator.backgroundColor = .systemBackground
        return indicator
    }()

    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.spacing = 8
        return stackView
    }()

    private(set) lazy var button: ProminentButton = {
        let button = ProminentButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addAction(.init(handler: { action in
            self.actionHandler?()
        }), for: .touchUpInside)
        return button
    }()

    private var actionHandler: (() -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .systemBackground
        setUpSubviews()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        if stackView.directionalLayoutMargins != directionalLayoutMargins {
            stackView.directionalLayoutMargins = directionalLayoutMargins
        }
    }

}

// MARK: - Public API -
public extension PlaceholderView {
    func showActivityIndicator(animated: Bool) {
        if activityIndicator.isHidden {
            activityIndicator.alpha = 0
            activityIndicator.isHidden = false
        }

        defer {
            bringSubviewToFront(activityIndicator)
            activityIndicator.startAnimating()
        }

        guard animated else {
            alpha = 1
            return
        }

        UIView.animate(withDuration: 0.3) {
            self.activityIndicator.alpha = 1
        }

    }

    func hide(animated: Bool) {
        hideView(activityIndicator, animated: true)
        hideView(self, animated: true)
        activityIndicator.stopAnimating()
    }

    func show(withTitle title: String?, message: String?, image: UIImage?, animated: Bool = true) {
        defer {
            setNeedsLayout()
        }

        hideView(activityIndicator, animated: animated)
        let configure: () -> Void = { [self] in
            if let title = title {
                titleLabel.isHidden = false
                titleLabel.text = title
            } else {
                titleLabel.isHidden = true
            }

            if let message = message {
                messageLabel.isHidden = false
                messageLabel.text = message
            } else {
                messageLabel.isHidden = true
            }

            if let image = image {
                imageView.isHidden = false
                imageView.image = image
            } else {
                hideView(imageView, animated: animated)
                imageView.isHidden = true
            }
        }

        guard animated else {
            configure()
            return
        }

        UIView.animate(withDuration: 0.33) {
            configure()
        }
    }

    func configureAction(withActionTitle title: String?, animated: Bool = true, handler: @escaping () -> Void) {
        defer {
            setNeedsLayout()
        }

        let configure: () -> Void = { [self] in
            if let title = title {
                actionHandler = {
                    handler()
                }
                button.setTitle(title, for: .normal)
                button.isHidden = false
            } else {
                button.isHidden = true
            }
        }
        guard animated else {
            configure()
            return
        }

        UIView.animate(withDuration: 0.3) {
            configure()
        }
    }
}

// MARK: - Private API -

private extension PlaceholderView {

    func hideView(_ view: UIView, animated: Bool) {
        guard animated else {
            view.isHidden = true
            return
        }

        UIView.animate(withDuration: 0.33) {
            view.alpha = 0
        } completion: { finished in
            guard finished else { return }
            view.isHidden = true
            view.alpha = 1
        }
    }

    func setUpSubviews() {
        setUpViewHierarchy()
        setUpConstraints()
    }

    func setUpViewHierarchy() {
        addSubview(stackView)
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(messageLabel)
        stackView.addArrangedSubview(button)
        button.isHidden = true

        addSubview(activityIndicator)
    }

    func setUpConstraints() {
        let guide = safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: guide.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: guide.trailingAnchor),
            stackView.centerYAnchor.constraint(equalTo: guide.centerYAnchor),
            // activity indicator
            activityIndicator.leadingAnchor.constraint(equalTo: leadingAnchor),
            activityIndicator.topAnchor.constraint(equalTo: guide.topAnchor),
            activityIndicator.trailingAnchor.constraint(equalTo: trailingAnchor),
            activityIndicator.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
