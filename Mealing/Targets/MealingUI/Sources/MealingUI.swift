import Foundation
import UIKit

public final class MealingUI {
    public static func hello() {
        print("Hello, from your UI framework")
    }
}

public final class ScrollableStackView: UIScrollView {

    // MARK: Lifecycle

    public init() {
        super.init(frame: .zero)
        setUpViews()
        setUpConstraints()
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private

    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()

    private var stackViewAxisConstraint: NSLayoutConstraint?
}

// MARK: - Public API -

public extension ScrollableStackView {
    var axis: NSLayoutConstraint.Axis {
      get { return stackView.axis }
      set {
        stackView.axis = newValue
        updateStackViewAxisConstraint()
      }
    }

    func addView(_ view: UIView, animated: Bool = false) {
        insertView(view, atIndex: stackView.arrangedSubviews.count, animated: animated)
    }
}

// MARK: - Private API -

private extension ScrollableStackView {

    func insertView(_ view: UIView, atIndex index: Int, animated: Bool) {
        let cell = ScrollableStackViewCell(contentView: view)
        stackView.insertArrangedSubview(cell, at: index)
        if animated {
            cell.alpha = 0
            layoutIfNeeded()
            UIView.animate(withDuration: 0.3) {
                cell.alpha = 1
            }
        }
    }

    func setUpViews() {
        setUpSelf()
        setUpViewHierarchy()
    }

    func setUpSelf() {
        backgroundColor = .systemBackground
    }

    func setUpViewHierarchy() {
        addSubview(stackView)
    }

    func setUpConstraints() {
        setUpStackViewConstraints()
        updateStackViewAxisConstraint()
    }

    func setUpStackViewConstraints() {
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
        ])
    }

    func updateStackViewAxisConstraint() {
        stackViewAxisConstraint?.isActive = false
        if stackView.axis == .vertical {
            stackViewAxisConstraint = stackView.widthAnchor.constraint(equalTo: widthAnchor)
        } else {
            stackViewAxisConstraint = stackView.heightAnchor.constraint(equalTo: heightAnchor)
        }
        stackViewAxisConstraint?.isActive = true
    }
}

final class ScrollableStackViewCell: UIView {
    // MARK: Lifecycle
    let contentView: UIView
    public init(contentView: UIView) {
      self.contentView = contentView

      super.init(frame: .zero)
      translatesAutoresizingMaskIntoConstraints = false
      if #available(iOS 11.0, *) {
        insetsLayoutMarginsFromSafeArea = false
      }

      setUpViews()
      setUpConstraints()
    }

    public required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
    }

    private func setUpViews() {
      setUpSelf()
      setUpContentView()
    }

    private func setUpSelf() {
      clipsToBounds = true
    }

    private func setUpContentView() {
      contentView.translatesAutoresizingMaskIntoConstraints = false
      addSubview(contentView)
    }

    private func setUpConstraints() {
      setUpContentViewConstraints()
    }

    private func setUpContentViewConstraints() {
      let bottomConstraint = contentView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor)
      bottomConstraint.priority = UILayoutPriority(rawValue: UILayoutPriority.required.rawValue - 1)

      NSLayoutConstraint.activate([
        contentView.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor),
        contentView.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor),
        contentView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor),
        bottomConstraint
      ])
    }
}
