
import Foundation

public struct MealCategory: Codable, Hashable {
    public init(idCategory: String, strCategory: String, strCategoryThumb: String, strCategoryDescription: String) {
        self.idCategory = idCategory
        self.strCategory = strCategory
        self.strCategoryThumb = strCategoryThumb
        self.strCategoryDescription = strCategoryDescription
    }

    public var idCategory: String
    public var strCategory: String
    public var strCategoryThumb: String
    public var strCategoryDescription: String
}
