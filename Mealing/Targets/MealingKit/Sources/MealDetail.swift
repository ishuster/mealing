
import Foundation

public struct MealDetail: Codable {
    public var idMeal: String
    public var strMeal: String
    public var strCategory: String
    public var strArea: String
    public var strInstructions: String
    public var strMealThumb: String
    public var ingredients: [String]
    public var measures: [String]

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: DynamicCodingKey.self)
        idMeal = try container.decode(String.self, forKey: .init(stringValue: "idMeal"))
        strMeal = try container.decode(String.self, forKey: .init(stringValue: "strMeal"))
        strCategory = try container.decode(String.self, forKey: .init(stringValue: "strCategory"))
        strArea = try container.decode(String.self, forKey: .init(stringValue: "strArea"))
        strInstructions = try container.decode(String.self, forKey: .init(stringValue: "strInstructions"))
        strMealThumb = try container.decode(String.self, forKey: .init(stringValue: "strMealThumb"))

        var ingredientsDictionary: [String: String] = [:]
        var ingredients: [String] = []

        var measuresDictionary: [String: String] = [:]
        var measures: [String] = []

        for key in container.allKeys where key.stringValue.contains("strIngredient") {
            if let ingredient = try? container.decode(String.self, forKey: DynamicCodingKey(stringValue: key.stringValue)) {
                ingredientsDictionary[key.stringValue] = ingredient
            }
        }

        for key in container.allKeys where key.stringValue.contains("strMeasure") {
            if let measure = try? container.decode(String.self, forKey: DynamicCodingKey(stringValue: key.stringValue)) {
                measuresDictionary[key.stringValue] = measure
            }
        }

        ingredientsDictionary.keys.sorted().forEach { key in
            guard let ingredient = ingredientsDictionary[key] else { return }
            ingredients.append(ingredient)
        }

        measuresDictionary.keys.sorted().forEach { key in
            guard let measure = measuresDictionary[key] else { return }
            measures.append(measure)
        }

        self.ingredients = ingredients
        self.measures = measures
    }

    private struct DynamicCodingKey: CodingKey {

        var stringValue: String
        var intValue: Int?

        init(stringValue: String) {
            self.stringValue = stringValue
        }

        init?(intValue: Int) {
            return nil
        }
    }
}
