
import Foundation

public struct Meal: Codable, Hashable {
    public var strMeal: String
    public var idMeal: String
    public var strMealThumb: String
}
