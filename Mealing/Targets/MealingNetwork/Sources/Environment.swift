import Foundation

public enum Environment {
    case dev
    case prod

    public static var current: Environment = .dev

    public var baseURLString: String {
        switch self {
        case .dev:
            return "https://www.themealdb.com"
        case .prod:
            return "https://www.themealdb.com"
        }
    }

    /// API Methods using the developer test key `1` as the API key
    /// See following [link](https://www.themealdb.com/api.php) to know more
    public var apiKey: String {
        switch self {
        case .dev:
            return "1"

        case .prod:
            return "1"
        }
    }
}
