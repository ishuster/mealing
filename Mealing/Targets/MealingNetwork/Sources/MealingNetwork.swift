import Foundation

public final class MealingNetworkConfiguration {
    public static func configure(environment: Environment) {
        Environment.current = environment
    }

    public static func makeNetworkServicing(session: URLSession) -> MealingNetworkServicing {
        return MealingNetworkService(session: session)
    }
}

public protocol MealingNetworkServicing {
    func request<Model: Codable>(endpoint: EndpointConvertible, completion: @escaping (Result<Model, Error>) -> Void)
}

final class MealingNetworkService: MealingNetworkServicing {
    let session: URLSession

    init(session: URLSession) {
        self.session = session
    }

    func request<Model: Codable>(endpoint: EndpointConvertible, completion: @escaping (Result<Model, Error>) -> Void) {
        do {
            let request = try endpoint.asURLRequest()
            session.dataTask(with: request) { data, _, error in
                if let error = error {
                    completion(.failure(error))
                    return
                }

                guard let data = data else {
                    completion(.failure(MealingNetworkServiceError.nilData))
                    return
                }

                if Environment.current == .dev {
                    if let json = data.prettyPrintedJSONString {
                        print("👀 response:\n\(json)", terminator: "\n\n")
                    }
                }

                do {
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(Model.self, from: data)
                    completion(.success(model))
                } catch {
                    completion(.failure(error))
                }
            }.resume()
        } catch {
            completion(.failure(error))
        }
    }
}


public enum MealingNetworkServiceError: Swift.Error {
    case nilData
}

private extension Data {
    var prettyPrintedJSONString: NSString? { /// NSString gives us a nice sanitized debugDescription
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else { return nil }

        return prettyPrintedString
    }
}
