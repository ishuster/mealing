import Foundation

public enum EndpointError: Swift.Error {
    case malformedUrl(string: String)
}
