import Foundation

public typealias Parameters = [String: Any]
public protocol EndpointConvertible {
    var apiKey: String { get }
    var baseUrlString: String { get }
    /// Path after API pattern
    ///
    /// Pattern followed:
    /// - `"/api/json/v1/<api_key>"`
    ///
    /// Provided path must start with a `/`
    /// - e.g. return `"/categories.php"`
    var path: String { get }
    /// Return API path
    ///
    /// e.g.
    /// - `"/api/json/v1/<api_key>"`
    var apiPath: String { get }
    var parameters: Parameters? { get }
    var httpMethod: HTTPMethod { get }
    func asURLRequest() throws -> URLRequest
}

public extension EndpointConvertible {
    var apiKey: String {
        return Environment.current.apiKey
    }

    var baseUrlString: String {
        return Environment.current.baseURLString
    }

    var apiPath: String {
        return "/api/json/v1/\(apiKey)"
    }

    func asURLRequest() throws -> URLRequest {
        guard var components = URLComponents(string: baseUrlString) else {
            throw EndpointError.malformedUrl(string: baseUrlString)
        }

        components.path = apiPath + path

        if let parameters = parameters, !parameters.isEmpty {
            let queryItems: [URLQueryItem] = parameters.map { key, value in URLQueryItem(name: key, value: "\(value)") }
            components.queryItems = queryItems
        }

        let url = components.url

        guard let anUrl = url else {
            throw EndpointError.malformedUrl(string: url?.absoluteString ?? baseUrlString)
        }
        return URLRequest(url: anUrl)
    }
}
